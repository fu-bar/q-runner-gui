﻿using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace QAssist.Utilities
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    public static class JsonHelper
    {
        public static string Serialize(object theObject, bool pretty = true)
        {
            string json = JsonConvert.SerializeObject(theObject);
            if (pretty) json = Format(json);
            return json;
        }

        public static T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static bool IsValid(string jsonToValidate)
        {
            if (string.IsNullOrWhiteSpace(jsonToValidate)) return false;

            jsonToValidate = jsonToValidate.Trim();
            if ((!jsonToValidate.StartsWith("{") || !jsonToValidate.EndsWith("}")) &&
                (!jsonToValidate.StartsWith("[") || !jsonToValidate.EndsWith("]"))) return false;
            try
            {
                JToken.Parse(jsonToValidate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string Format(string jsonToPretty)
        {
            var jToken = JToken.Parse(jsonToPretty);
            return JsonConvert.SerializeObject(jToken, Formatting.Indented);
        }

        public static string Escape(string json)
        {
            return JsonConvert.ToString(json);
        }

        public static string Unescape(string json)
        {
            return Regex.Unescape(json);
        }
    }
}