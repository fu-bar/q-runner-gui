﻿using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Text;

namespace QAssist.Utilities
{
    // ReSharper disable once InconsistentNaming
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    public class RESTHelper
    {
        private static readonly HttpClient HttpClient = new HttpClient();

        public static HttpResponseMessage Post(string json, string address)
        {
            HttpResponseMessage response = HttpClient.PostAsync(
                address,
                new StringContent(
                    json,
                    Encoding.UTF8,
                    "application/json")
            ).Result;

            return response;
        }

        public static HttpResponseMessage Get(string address)
        {
            HttpResponseMessage response = HttpClient.GetAsync(address).Result;
            return response;
        }
    }
}