﻿using System.IO;
using Microsoft.Win32;
using Notifications.Wpf;

namespace QAssist.Utilities
{
    public static class Persistence
    {
        public static void SaveToFile(string content, string filter = "JSON file (*.json)|*.json")
        {
            var saveFileDialog = new SaveFileDialog {Filter = filter};
            if (saveFileDialog.ShowDialog() != true) return;
            var fileName = saveFileDialog.FileName;
            File.WriteAllText(fileName, content);
            Balloons.ShowBalloon("File Saved", $"{fileName}");
        }

        public static string ReadFromFile(string filePath)
        {
            var fileContent = File.ReadAllText(filePath);
            return fileContent;
        }

        public static string LoadFromFile(out string selectedFile, string filter = "JSON file (*.json)|*.json")
        {
            var openFileDialog = new OpenFileDialog {Filter = filter};
            var fileSelected = openFileDialog.ShowDialog();
            if (fileSelected != true) Balloons.ShowBalloon("Canceled", "No file selected", NotificationType.Warning);
            selectedFile = openFileDialog.FileName;
            return fileSelected != true ? null : File.ReadAllText(openFileDialog.FileName);
        }
    }
}