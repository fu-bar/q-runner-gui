﻿using System;

namespace QAssist.Utilities
{
    public class Utils
    {
        public static string Timestamped(string str)
        {
            return $"[{DateTime.Now:HH:mm:ss}] {str}";
        }
    }
}