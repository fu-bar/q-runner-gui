﻿using Notifications.Wpf;

namespace QAssist.Utilities
{
    public static class Balloons
    {
        private static readonly Notifications.Wpf.NotificationManager NotificationManager =
            new Notifications.Wpf.NotificationManager();

        public static void ShowBalloon(string title, string message, NotificationType notificationType = NotificationType.Information)
        {
            NotificationManager.Show(new NotificationContent
            {
                Title = title,
                Message = message,
                Type = notificationType
            });
        }
    }
}