﻿using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using QAssist.Annotations;
using QAssist.Utilities;
using QAssist.ViewModels;

namespace QAssist
{
    public sealed partial class AppWindow : INotifyPropertyChanged
    {
        private Session _session;

        public AppWindow()
        {
            InitializeComponent();
            Session = new Session();
        }

        public Session Session
        {
            get => _session;
            set
            {
                _session = value;
                OnPropertyChanged(nameof(Session));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void TextBoxBase_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Responses.ScrollToEnd();
        }

        private void SaveSession(object sender, RoutedEventArgs e)
        {
            Persistence.SaveToFile(JsonHelper.Serialize(_session));
        }

        private void LoadSession(object sender, RoutedEventArgs e)
        {
            var jsonContent = Persistence.LoadFromFile(out var sessionFilePath);
            if (sessionFilePath != "")
                Title = $"Session: {sessionFilePath}";
            else
                return;
            Session.FinalizeSession();
            Session = JsonHelper.Deserialize<Session>(jsonContent);
            Session.SetStatus($"Session: {sessionFilePath}");
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void RequestDataGrid_OnLoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex().ToString();
        }

        private void OnAppLoaded(object sender, RoutedEventArgs e)
        {
            const string defaultSession = "resources/defaults.json";
            if (!File.Exists(defaultSession)) return;
            Session = JsonHelper.Deserialize<Session>(Persistence.ReadFromFile(defaultSession));
            Session.SetStatus($"Session: {defaultSession}");
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            _session.FinalizeSession();
            Application.Current.Shutdown();
        }

        private void RequestBodyChanged(object sender, TextChangedEventArgs e)
        {
            Session.OnPropertyChanged("IsRequestInvalid");
        }
    }
}