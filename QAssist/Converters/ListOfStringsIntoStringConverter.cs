﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace QAssist.Converters
{
    internal class ListOfStringsIntoStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Join(",", value as List<string> ?? new List<string> {"OOPS"});
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value as string)?.Split(',').Select(tag => tag.Trim()).ToList();
        }
    }
}