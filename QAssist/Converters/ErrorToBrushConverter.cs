﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace QAssist.Converters
{
    public class ErrorToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && (bool) value) return Brushes.OrangeRed;

            return Brushes.ForestGreen;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}