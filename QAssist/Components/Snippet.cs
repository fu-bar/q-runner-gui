﻿using System.Collections.Generic;

namespace QAssist.Components
{
    public class Snippet : JsonBody
    {
        private List<string> _tags = new List<string>();

        public List<string> Tags
        {
            get => _tags;
            set
            {
                _tags = value;
                OnPropertyChanged(nameof(Tags));
            }
        }
    }
}