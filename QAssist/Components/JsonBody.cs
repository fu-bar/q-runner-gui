﻿using QAssist.Utilities;
using QAssist.ViewModels;

namespace QAssist.Components
{
    public class JsonBody : ObservableObject
    {
        private string _body;
        private string _description;

        public string Body
        {
            get => _body;
            set
            {
                _body = value;
                OnPropertyChanged(nameof(Body));
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public bool IsValid()
        {
            return Body != null && JsonHelper.IsValid(Body);
        }
    }
}