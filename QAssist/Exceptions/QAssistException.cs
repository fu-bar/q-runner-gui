﻿using System;

namespace QAssist.Exceptions
{
    public class QAssistException : Exception
    {
        public QAssistException(string message) : base(message)
        {
        }

        public QAssistException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}