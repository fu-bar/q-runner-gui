﻿using System.Collections.Generic;
using QAssist.ViewModels;

namespace QAssist.Admin
{
    public class Config : ObservableObject
    {
        private readonly Dictionary<string, object> _config = new Dictionary<string, object>();

        public Config()
        {
            _config[nameof(RequestAddress)] = "http://localhost:3300/api/v1/command/application/app1/command";
            _config[nameof(ResultsAddress)] = "http://localhost:3300/api/v1/command/response";
            _config[nameof(PullResultsIntervalMilliseconds)] = 500;
            _config[nameof(IsAutoPollingActive)] = true;
        }

        public Config(string requestAddress, string resultsAddress, int pullResultsIntervalMilliseconds, bool isAutoPollingActive)
        {
            _config[nameof(RequestAddress)] = requestAddress;
            _config[nameof(ResultsAddress)] = resultsAddress;
            _config[nameof(PullResultsIntervalMilliseconds)] = pullResultsIntervalMilliseconds;
            _config[nameof(IsAutoPollingActive)] = isAutoPollingActive;
        }

        public string RequestAddress
        {
            get => (string) _config[nameof(RequestAddress)];
            set
            {
                _config[nameof(RequestAddress)] = value;
                OnPropertyChanged(nameof(RequestAddress));
            }
        }

        public string ResultsAddress
        {
            get => (string) _config[nameof(ResultsAddress)];
            set
            {
                _config[nameof(ResultsAddress)] = value;
                OnPropertyChanged(nameof(ResultsAddress));
            }
        }

        public int PullResultsIntervalMilliseconds
        {
            get => (int) _config[nameof(PullResultsIntervalMilliseconds)];
            set
            {
                _config[nameof(PullResultsIntervalMilliseconds)] = value;
                OnPropertyChanged(nameof(PullResultsIntervalMilliseconds));
            }
        }

        public bool IsAutoPollingActive
        {
            get => (bool) _config[nameof(IsAutoPollingActive)];
            set
            {
                _config[nameof(IsAutoPollingActive)] = value;
                OnPropertyChanged(nameof(IsAutoPollingActive));
            }
        }
    }
}