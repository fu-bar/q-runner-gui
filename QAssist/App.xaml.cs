﻿using System.Windows;
using System.Windows.Threading;

namespace QAssist
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private void UnhandledExceptionHandler(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, e.Exception.Source, MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
            Window errorWindow =
                new TextWindow(
                    e.Exception.Message,
                    e.Exception.StackTrace
                );
            errorWindow.Show();
            errorWindow.Topmost = true;
        }
    }
}