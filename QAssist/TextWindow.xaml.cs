﻿using System.Windows;

namespace QAssist
{
    public partial class TextWindow : Window
    {
        public TextWindow(string title, string message)
        {
            InitializeComponent();
            Title = title;
            ErrorBodyTextBox.Text = message;
        }
    }
}