﻿using System;
using System.Collections.ObjectModel;
using System.Media;
using System.Net.Http;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Newtonsoft.Json;
using Notifications.Wpf;
using QAssist.Admin;
using QAssist.Commands;
using QAssist.Components;
using QAssist.Utilities;
using Serilog;
using Serilog.Core;

namespace QAssist.ViewModels
{
    public class Session : ObservableObject
    {
        private const string DateFormat = "HH:mm:ss [d/MM/yyyy]";
        private ICommand _clearResponsesCommand;
        private Config _configuration;
        private ICommand _deleteRequestCommand;
        private ICommand _exportResponsesCommand;

        private ICommand _formatCommand;
        private bool _inErrorState;
        private FlowDocument _log;
        private Logger _logger;
        private ICommand _newRequestCommand;
        private DispatcherTimer _responsePoller;
        private FlowDocument _responses = new FlowDocument();
        private ICommand _reverseStringificationCommand;
        private ICommand _runAllCommand;
        private ICommand _runRequestCommand;
        private ICommand _runToSelectionCommand;
        private int _selectedRequestIndex;
        private ICommand _stringifyCommand;
        private string _userMessage;

        public Session(Config config)
        {
            Init();
            _logger.Debug("Loading configuration");
            Configuration = config;
        }

        public Session()
        {
            Init();
            _logger.Debug("Starting with default configuration");
            Configuration = new Config();
            StartPolling();
        }

        public ObservableCollection<JsonBody> Requests { get; } = new ObservableCollection<JsonBody>();

        public bool InErrorState
        {
            get => _inErrorState;
            set
            {
                _inErrorState = value;
                OnPropertyChanged(nameof(InErrorState));
            }
        }

        public bool IsRequestInvalid
        {
            get
            {
                var isInvalid = !JsonHelper.IsValid(Requests[SelectedRequestIndex].Body);
                return isInvalid;
            }
        }

        public int SelectedRequestIndex
        {
            get => _selectedRequestIndex;
            set
            {
                _selectedRequestIndex = value;
                OnPropertyChanged(nameof(SelectedRequestIndex));
            }
        }


        public Config Configuration
        {
            get => _configuration;
            set
            {
                _configuration = value;
                OnPropertyChanged(nameof(Configuration));
            }
        }

        [JsonIgnore]
        public FlowDocument Responses
        {
            get => _responses;
            set
            {
                _responses = value;
                OnPropertyChanged(nameof(Responses));
            }
        }

        [JsonIgnore]
        public FlowDocument Log
        {
            get => _log;
            set
            {
                _log = value;
                OnPropertyChanged(nameof(Log));
            }
        }

        public string UserMessage
        {
            get => _userMessage;
            set
            {
                _userMessage = value;
                OnPropertyChanged(nameof(UserMessage));
            }
        }

        private static void ReverseStringification(object obj)
        {
            if (!(obj is TextBox tb)) return;
            var unescaped = JsonHelper.Unescape(tb.SelectedText);
            if (unescaped.StartsWith("\"")) unescaped = unescaped.Remove(0, 1);
            if (unescaped.EndsWith("\"")) unescaped = unescaped.Remove(unescaped.Length - 1, 1);
            tb.Text = tb.Text.Replace(tb.SelectedText, unescaped);
        }

        private static void Stringify(object obj)
        {
            if (obj is TextBox tb) tb.Text = tb.Text.Replace(tb.SelectedText, JsonHelper.Escape(tb.SelectedText));
        }

        public void FinalizeSession()
        {
            _logger.Information("Finalizing Session");
            _responsePoller.Stop();
            _responsePoller.IsEnabled = false;
        }

        private void ExportResponses(object obj)
        {
            var responsesString = new TextRange(_responses.ContentStart, _responses.ContentEnd).Text;
            Persistence.SaveToFile(responsesString, "TXT file (*.txt)|*.txt");
        }

        private void ClearResponses(object obj)
        {
            _responses.Blocks.Clear();
            OnPropertyChanged(nameof(Responses));
        }

        private void Init()
        {
            _logger = new LoggerConfiguration().WriteTo.File(
                    @"logs/qa_assist.log", rollingInterval: RollingInterval.Day,
                    rollOnFileSizeLimit: true,
                    fileSizeLimitBytes: 1000000)
                .MinimumLevel.Debug()
                .CreateLogger();
        }

        private void Beautify(object obj)
        {
            foreach (var request in Requests)
            {
                try
                {
                    request.Body = JsonHelper.Format(request.Body);
                }
                catch (Exception)
                {
                    // JSON may be invalid
                }

                if (!JsonHelper.IsValid(Requests[_selectedRequestIndex].Body))
                    SetStatus("Can't format an invalid JSON", true);
            }
        }

        private void StartPolling()
        {
            if (_responsePoller != null && _responsePoller.IsEnabled)
            {
                _logger.Debug("Stopping an active response poller");
                _responsePoller.Stop();
                _responsePoller.IsEnabled = false;
            }

            var pollInterval = Configuration.PullResultsIntervalMilliseconds;
            _responsePoller = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(pollInterval) };
            _responsePoller.Tick += PollResponses;
            _logger.Debug($"Starting response polling every {pollInterval} milliseconds");
            _responsePoller.Start();
        }

        private void PollResponses(object sender, EventArgs e)
        {
            HttpResponseMessage response;
            try
            {
                response = RESTHelper.Get(Configuration.ResultsAddress);
            }
            catch (Exception exception)
            {
                var errorMessage = $"{exception.Message}: failed fetching from: {Configuration.ResultsAddress}";
                Error(errorMessage, "Error Fetching Response");
                _logger.Debug("Stopping response poller");
                _responsePoller.Stop();
                return;
            }

            var responseBody = response.Content.ReadAsStringAsync().Result.Trim();
            if (responseBody == "[]") return;

            var responseContainsException = responseBody.ToLower().Contains("stacktrace");
            if (responseContainsException) Error("Response contains a stacktrace...", "Exception In Response!");

            responseBody = PostProcessResponse(JsonHelper.Format(responseBody));

            var paragraph = new Paragraph
            {
                Background = responseContainsException ? Brushes.OrangeRed : Brushes.LightGreen,
                TextAlignment = TextAlignment.Center
            };
            Inline timestamp = new Run(DateTime.Now.ToString(DateFormat));
            paragraph.Inlines.Add(timestamp);
            Responses.Blocks.Add(paragraph);
            paragraph = new Paragraph();
            Inline body = new Run(responseBody);
            paragraph.Inlines.Add(body);
            Responses.Blocks.Add(paragraph);
        }

        private void Output(string message, Brush captionColor)
        {
            var paragraph = new Paragraph
            {
                Background = captionColor,
                TextAlignment = TextAlignment.Center
            };
            Inline timestamp = new Run(DateTime.Now.ToString(DateFormat));
            paragraph.Inlines.Add(timestamp);
            Responses.Blocks.Add(paragraph);
            paragraph = new Paragraph();
            Inline body = new Run(message);
            paragraph.Inlines.Add(body);
            Responses.Blocks.Add(paragraph);
        }

        public void SetStatus(string userMessage, bool inErrorState = false)
        {
            UserMessage = Utils.Timestamped(userMessage);
            InErrorState = inErrorState;
            if (inErrorState) SystemSounds.Exclamation.Play();
        }


        #region Commands

        public ICommand RunRequestCommand => _runRequestCommand ?? (_runRequestCommand = new RelayCommand(RunRequest));

        public ICommand RunToSelectionCommand =>
            _runToSelectionCommand ?? (_runToSelectionCommand = new RelayCommand(RunToSelection));

        public ICommand RunAllCommand => _runAllCommand ?? (_runAllCommand = new RelayCommand(RunAll));
        public ICommand FormatCommand => _formatCommand ?? (_formatCommand = new RelayCommand(Beautify));
        public ICommand StringifyCommand => _stringifyCommand ?? (_stringifyCommand = new RelayCommand(Stringify));
        public ICommand NewRequestCommand => _newRequestCommand ?? (_newRequestCommand = new RelayCommand(NewRequest));

        private void NewRequest(object obj)
        {
            throw new NotImplementedException();
        }

        public ICommand DeleteRequestCommand =>
            _deleteRequestCommand ?? (_deleteRequestCommand = new RelayCommand(DeleteRequest));

        private void DeleteRequest(object obj)
        {
            if (SelectedRequestIndex < 0 || SelectedRequestIndex >= Requests.Count)
            {
                SetStatus("No selected request to delete", true);
                return;
            }

            Requests.RemoveAt(SelectedRequestIndex);
            SelectedRequestIndex -= 1;
        }

        public ICommand ReverseStringificationCommand =>
            _reverseStringificationCommand ??
            (_reverseStringificationCommand = new RelayCommand(ReverseStringification));

        public ICommand ExportResponsesCommand =>
            _exportResponsesCommand ?? (_exportResponsesCommand = new RelayCommand(ExportResponses));

        public ICommand ClearResponsesCommand =>
            _clearResponsesCommand ?? (_clearResponsesCommand = new RelayCommand(ClearResponses));

        private void RunRequest(object requestBody)
        {
            var request = requestBody as string;
            if (!JsonHelper.IsValid(request))
            {
                var errorMessage = $"Failed Parsing:\n{request}";
                Error(errorMessage, "Invalid JSON Request");
                return;
            }

            try
            {
                var response = RESTHelper.Post(request, _configuration.RequestAddress);
                if (!response.IsSuccessStatusCode)
                {
                    var errorMessage = $"Got reply with status: {response.StatusCode.ToString()}";
                    SetStatus(errorMessage, true);
                    return;
                }
            }
            catch (Exception exception)
            {
                Error(exception.Message);
                _logger.Error($"{exception}");
                return;
            }

            Output(JsonHelper.Format(request), Brushes.Wheat);

            SetStatus("Request sent");
            if (!_responsePoller.IsEnabled) StartPolling();

            if (_selectedRequestIndex != Requests.Count - 1) SelectedRequestIndex += 1;
        }

        private void RunToSelection(object requestBody)
        {
            var runTo = SelectedRequestIndex;
            for (var index = 0; index <= runTo; index++)
            {
                SelectedRequestIndex = index;
                RunRequest(Requests[index].Body);
                if (!InErrorState) continue;
                _logger.Error($"Failed with request:\n{JsonHelper.Format(Requests[index].Body)}");
                SetStatus($"Failed execution on step: {index} ({Requests[index].Description})", true);
                return;
            }
        }

        private void RunAll(object requestBody)
        {
            SelectedRequestIndex = Requests.Count - 1;
            RunToSelection(null);
        }

        private string PostProcessResponse(string response)
        {
            var adaptedResponse = response
                .Replace("\\t", "\t")
                .Replace("\\\"", "\"")
                .Replace("\\r", "\r")
                .Replace("\\n", "\n");
            return adaptedResponse;
        }

        private void Error(string errorMessage, string title = "Error")
        {
            _logger.Error($"{title} : {errorMessage}");
            Balloons.ShowBalloon(title, errorMessage, NotificationType.Error);
            SystemSounds.Exclamation.Play();
            SetStatus(title, true);
        }

        #endregion
    }
}